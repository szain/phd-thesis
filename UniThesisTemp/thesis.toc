\contentsline {chapter}{Nomenclature}{xvi}{chapter*.5}%
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Aims and Contributions}{2}{section.1.1}%
\contentsline {section}{\numberline {1.2}Thesis structure}{4}{section.1.2}%
\contentsline {chapter}{\numberline {2}Digital Signature Schemes}{7}{chapter.2}%
\contentsline {section}{\numberline {2.1}Introduction}{7}{section.2.1}%
\contentsline {section}{\numberline {2.2}Digital Signatures}{8}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Mathematical Primitives}{10}{subsection.2.2.1}%
\contentsline {subsubsection}{Computation over Prime Field}{10}{section*.6}%
\contentsline {subsubsection}{Cyclic Group}{11}{section*.7}%
\contentsline {subsection}{\numberline {2.2.2}RSA signatures}{11}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Plain RSA}{12}{subsection.2.2.3}%
\contentsline {subsection}{\numberline {2.2.4}Signatures from Hash Functions}{13}{subsection.2.2.4}%
\contentsline {section}{\numberline {2.3}Exploring Cryptographic Concepts}{13}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Hash Function}{13}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Random Oracle model (ROM)}{14}{subsection.2.3.2}%
\contentsline {subsection}{\numberline {2.3.3}Distinguishing the Given Scheme}{16}{subsection.2.3.3}%
\contentsline {subsubsection}{Randomness}{16}{section*.8}%
\contentsline {subsection}{\numberline {2.3.4}Discrete-Logarithm}{17}{subsection.2.3.4}%
\contentsline {subsection}{\numberline {2.3.5}The Schnorr Signature Scheme}{18}{subsection.2.3.5}%
\contentsline {subsection}{\numberline {2.3.6}Schnorr Identification Scheme}{19}{subsection.2.3.6}%
\contentsline {subsection}{\numberline {2.3.7}Zero-Knowledge Proofs (ZKPs)}{20}{subsection.2.3.7}%
\contentsline {section}{\numberline {2.4}Background Definitions}{28}{section.2.4}%
\contentsline {subsubsection}{Code-Based Game-Playing Proofs and Exact Security}{29}{section*.9}%
\contentsline {subsubsection}{\textsf {EasyCrypt}\xspace }{30}{section*.10}%
\contentsline {subsubsection}{\textsf {pRHL}\xspace Judgments}{30}{section*.11}%
\contentsline {subsubsection}{\textsf {pHL}\xspace Judgments}{31}{section*.13}%
\contentsline {subsection}{\numberline {2.4.1}The Computational Diffie-Hellman Assumption}{32}{subsection.2.4.1}%
\contentsline {subsection}{\numberline {2.4.2}Signature Scheme}{33}{subsection.2.4.2}%
\contentsline {subsection}{\numberline {2.4.3}\textsf {EUF\textrm {-}CMA}\xspace security in the ROM}{35}{subsection.2.4.3}%
\contentsline {section}{\numberline {2.5}Discussion}{36}{section.2.5}%
\contentsline {chapter}{\numberline {3}Transforming security proofs into semantic framework games}{39}{chapter.3}%
\contentsline {section}{\numberline {3.1}Introduction}{39}{section.3.1}%
\contentsline {section}{\numberline {3.2}Formalisms for Security Definitions}{40}{section.3.2}%
\contentsline {section}{\numberline {3.3}The formal semantics of Programming language}{41}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Summary of Notation}{41}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}Operational Semantics}{42}{subsection.3.3.2}%
\contentsline {subsubsection}{Rules for commands}{43}{section*.14}%
\contentsline {subsection}{\numberline {3.3.3}Denotational semantics}{43}{subsection.3.3.3}%
\contentsline {subsection}{\numberline {3.3.4}Axiomatic semantics}{43}{subsection.3.3.4}%
\contentsline {section}{\numberline {3.4}Game-based proofs}{44}{section.3.4}%
\contentsline {subsection}{\numberline {3.4.1}Transition based on indistinguishability}{47}{subsection.3.4.1}%
\contentsline {subsection}{\numberline {3.4.2}Transitions based on failure events}{47}{subsection.3.4.2}%
\contentsline {subsection}{\numberline {3.4.3}Bridging step}{48}{subsection.3.4.3}%
\contentsline {section}{\numberline {3.5}Proof-assistants for Cryptography}{49}{section.3.5}%
\contentsline {section}{\numberline {3.6}\textsf {EasyCrypt}\xspace proof assistant}{51}{section.3.6}%
\contentsline {subsubsection}{Probabilistic Relational Hoare Logic (\textsf {pRHL}\xspace )}{52}{section*.15}%
\contentsline {subsubsection}{Probabilistic Hoare Logic (\textsf {pHL}\xspace )}{53}{section*.16}%
\contentsline {subsubsection}{Hoare logic (HL)}{53}{section*.17}%
\contentsline {subsection}{\numberline {3.6.1}A primer on \textsf {EasyCrypt}\xspace }{54}{subsection.3.6.1}%
\contentsline {subsection}{\numberline {3.6.2}Input Language}{54}{subsection.3.6.2}%
\contentsline {subsection}{\numberline {3.6.3}Axioms and Lemmas}{55}{subsection.3.6.3}%
\contentsline {subsection}{\numberline {3.6.4}Game declarations}{56}{subsection.3.6.4}%
\contentsline {chapter}{\numberline {4}Machine-Checked verification of \textsf {EDL}\xspace and \textsf {CM}\xspace Signature Schemes}{59}{chapter.4}%
\contentsline {section}{\numberline {4.1}Introduction}{59}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Our Contributions}{60}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Related Work}{60}{subsection.4.1.2}%
\contentsline {section}{\numberline {4.2}Mathematical Preliminaries}{62}{section.4.2}%
\contentsline {section}{\numberline {4.3}\textsf {EDL}\xspace Signatures and their Security}{62}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Intuition}{65}{subsection.4.3.1}%
\contentsline {subsection}{\numberline {4.3.2}Formalisation}{66}{subsection.4.3.2}%
\contentsline {subsubsection}{Refactoring}{67}{section*.20}%
\contentsline {subsubsection}{Embedding}{68}{section*.21}%
\contentsline {subsubsection}{Simulation}{69}{section*.22}%
\contentsline {subsubsection}{Reduction}{72}{section*.23}%
\contentsline {section}{\numberline {4.4}\textsf {CM}\xspace Signatures and their Security}{74}{section.4.4}%
\contentsline {subsection}{\numberline {4.4.1}Proof Overview}{77}{subsection.4.4.1}%
\contentsline {subsection}{\numberline {4.4.2}Formalisation}{78}{subsection.4.4.2}%
\contentsline {subsubsection}{Step 0 — Embedding}{78}{section*.24}%
\contentsline {subsubsection}{Step 0 — Simulation}{80}{section*.25}%
\contentsline {subsubsection}{Step 0 — Reduction}{80}{section*.26}%
\contentsline {section}{\numberline {4.5}Conclusion}{84}{section.4.5}%
\contentsline {section}{\numberline {4.6}Further Generalisations}{86}{section.4.6}%
\contentsline {chapter}{\numberline {5}Machine-checked verification of \textsf {GJKW}\xspace }{89}{chapter.5}%
\contentsline {section}{\numberline {5.1}Introduction}{89}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Contribution}{91}{subsection.5.1.1}%
\contentsline {subsection}{\numberline {5.1.2}Related Work}{91}{subsection.5.1.2}%
\contentsline {subsection}{\numberline {5.1.3}Outline}{92}{subsection.5.1.3}%
\contentsline {section}{\numberline {5.2}\textsf {GJKW}\xspace signature scheme}{92}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}\textsf {GJKW}\xspace in \textsf {EasyCrypt}\xspace format}{93}{subsection.5.2.1}%
\contentsline {section}{\numberline {5.3}Security}{96}{section.5.3}%
\contentsline {section}{\numberline {5.4}Formalisation}{99}{section.5.4}%
\contentsline {subsection}{\numberline {5.4.1}Proof Overview}{99}{subsection.5.4.1}%
\contentsline {subsection}{\numberline {5.4.2}The Sequence of Games}{100}{subsection.5.4.2}%
\contentsline {subsubsection}{[\(\textsf {EUF\textrm {-}CMA}\xspace (\EuScript {H}\xspace ,\EuScript {G}\xspace ,\textsf {GJKW}\xspace , \EuScript {F})\)]}{101}{section*.30}%
\contentsline {subsubsection}{[\(Game_0^{\textsf {GJKW}\xspace }(\EuScript {H}\xspace ,\EuScript {G}\xspace ,\EuScript {S}\xspace )\)]}{101}{section*.31}%
\contentsline {subsubsection}{1st Simplification}{103}{section*.32}%
\contentsline {subsubsection}{[\(Game_{bad}^{\textsf {GJKW}\xspace }(\EuScript {H}\xspace ,\EuScript {G}\xspace ,\EuScript {S}\xspace )\) : bad] (failure event)}{104}{section*.33}%
\contentsline {subsubsection}{Failure Event Lemma (fel tactic)}{106}{section*.34}%
\contentsline {subsubsection}{2nd Simplification}{108}{section*.35}%
\contentsline {section}{\numberline {5.5}Formal Steps}{110}{section.5.5}%
\contentsline {subsubsection}{Step 1: Refactorisation (\(Game_1^\textsf {GJKW}\xspace (\EuScript {F})\) and \(Shim(O_0)^\textsf {GJKW}\xspace (\EuScript {F})\))}{111}{section*.36}%
\contentsline {subsubsection}{3 Sub-Steps}{111}{section*.37}%
\contentsline {subsubsection}{[\(Shim(O_0)^{\textsf {GJKW}\xspace }(\EuScript {H}\xspace ,\EuScript {G}\xspace ,\EuScript {S}\xspace )\)]}{112}{section*.38}%
\contentsline {subsubsection}{Step 2: Embedding and Simulation \((Shim(O_1)^\textsf {GJKW}\xspace (\EuScript {H}\xspace , \EuScript {G}\xspace ,CompRel))\)}{113}{section*.39}%
\contentsline {subsubsection}{[\(Shim(O_1)^\textsf {GJKW}\xspace (\EuScript {H}\xspace , \EuScript {G}\xspace ,CompRel) \)]}{115}{section*.40}%
\contentsline {subsubsection}{Step 3: Reduction }{115}{section*.41}%
\contentsline {section}{\numberline {5.6}Reflecting on the proof pattern}{117}{section.5.6}%
\contentsline {section}{\numberline {5.7}Lessons Learned}{119}{section.5.7}%
\contentsline {chapter}{\numberline {6}Conclusion}{121}{chapter.6}%
\contentsline {section}{\numberline {6.1}Future Work}{124}{section.6.1}%
\contentsline {chapter}{\numberline {A}}{127}{appendix.A}%
\contentsline {chapter}{Bibliography}{129}{figure.A.1}%
